//Load Express Library
var express = require("express");

//Create an instance of the Express application
var app = express();

//Configure the routes
//Check and match request

app.use("/index.html", function(req, res){
    res.status(200);
    res.type("text/html");
    res.send("<h1>Hello</h1>");
});

app.use(express.static(__dirname + "/public"));
app.use("/images",express.static(__dirname + "/pics"));

//Create my server 
//Specify the port that the app wil be listening to
var port = 3000;

//Bind the app to the port
app.listen(port,function(){
    console.log("Application started on port %d", port);
});

